package com.zsk.tool.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class UI extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private JPanel panel;

    private int startX = 300;
    private int startY = 450;
    private int width = 200;
    private int hAdd = 2;
    private int h = 0;
    private int posX = startX;

    boolean goRight = true;

    public void init() {
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        panel = new JPanel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                // TODO Auto-generated method stub
                super.paintComponent(g);
                // Graphics2D g2d = (Graphics2D)g;
                draw((Graphics2D)g);
            }
        };
        add(panel);
        setVisible(true);
        validate();

        new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (posX > startX + width) {
                        goRight = false;
                        //posX = startX;
                        h += hAdd;
                    }
                    if (posX <= startX) {
                        goRight = true;
                        h += hAdd;
                    }
                    if (goRight) {
                        posX += 2;
                    } else {
                        posX -= 2;
                    }

                    // h += hAdd;
                    panel.repaint();
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            };
        }.start();
    }

    private void draw(Graphics2D g) {
        g.drawLine(0, startY, 800, startY);
        g.setPaint(Color.BLACK);
        g.fillRect(startX, startY - h, width, h);
        
        if(goRight) {
            g.fillRect(startX, startY - h - hAdd, (posX - startX), hAdd);
        }else {
            g.fillRect(posX, startY - h - hAdd, (startX+width -posX), hAdd);
        }
       
        // 绘制笔
        g.setPaint(Color.RED);
        // g.fillRect(posX, startY - h - 40 - hAdd, 5, 40);

        int xs = posX;
        int ys = startY - h - hAdd;
        g.fillRect(xs - 8, ys - 48, 16, 40);
        int[] xss = {xs, xs - 8, xs + 8};
        int[] yss = {ys, ys - 8, ys - 8};
        g.drawPolygon(xss, yss, 3);

    }

    private void drawLight(Graphics2D g) {
        g.setPaint(Color.LIGHT_GRAY);
        g.drawLine(100, startY, 700, startY);
        g.drawLine(100, startY, 100, startY - 50);
        g.drawLine(700, startY, 700, startY - 50);

        g.setPaint(Color.BLACK);
        g.fillRect(startX, startY - h - hAdd, width, h);
        g.setPaint(Color.YELLOW);
        g.fillRect(startX, startY - hAdd, width, hAdd);

        // 绘制光线
        g.setPaint(Color.RED);

        g.drawLine(startX, startY + 30, startX + width, startY + 30);
        for (int i = 0; i < width / 5; i++) {
            g.drawLine(startX + i * 5, startY + 30, startX + i * 5, startY + 20);
        }
        // // g.fillRect(posX, startY - h - 40 - hAdd, 5, 40);
        //
        // int xs = posX;
        // int ys = startY - h - hAdd;
        // g.fillRect(xs-8, ys-48, 16, 40);
        // int[] xss = {xs, xs - 8, xs + 8};
        // int[] yss = {ys, ys - 8, ys - 8};
        // g.drawPolygon(xss, yss, 3);

    }

    public static void main(String[] args) {
        new UI().init();
    }

}
