package com.zsk.tool.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * 文件读取
 * 
 * @author keke
 * @date 2020/11/13
 */
public class FileReaderUtil {
    /**
     * 按行读文件
     * 
     * @param filePath
     */
    public static void readFileByLine(String filePath) {
        File file = new File(filePath);
        try (BufferedReader br = new BufferedReader(new FileReader(file));) {
            String line = null;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
