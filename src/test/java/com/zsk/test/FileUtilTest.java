package com.zsk.test;

import com.zsk.tool.uuid.UUIDUtil;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author: keke
 * @date: 2023/5/31
 */
public class FileUtilTest {

    @Before
    public void init() {

    }

    @Test
    public void getOneTest() throws IOException {
        String path1 = "../b/c";
        String path2 = "./c";
        String path3 = "a/b/../c";
        System.out.println(FileUtils.getFile(path1).getCanonicalPath());
        System.out.println(FileUtils.getFile(path2).getCanonicalPath());
        System.out.println(FileUtils.getFile(path3).getCanonicalPath());
    }

}
