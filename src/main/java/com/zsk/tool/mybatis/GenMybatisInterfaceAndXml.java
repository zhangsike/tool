package com.zsk.tool.mybatis;

import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: keke
 * @date: ${DATE} ${TIME}
 */
public class GenMybatisInterfaceAndXml {
    public static void main(String[] args) throws IOException {
        List<String> list = FileUtils.readLines(FileUtils.getFile("F:/idea-prj/mybatis-normal-demo/src/main/java/com/zsk/entity/AttrInfo.java"), StandardCharsets.UTF_8);
        Set<String> baseTypeSet = Stream.of(
                "Integer",
                "int",
                "String",
                "Long",
                "long",
                "Date",
                "Boolean",
                "boolean").collect(Collectors.toSet());
        List<String> tmpList = new ArrayList<>();
        for (String str : list) {
            String[] strs = str.split(" ");
            int pos = -1;
            int index = 0;
            for (String s : strs) {
                if (s.length() > 0) {
                    if(s.equals("private")) {
                        pos = index;
                        break;
                    } else {
                        break;
                    }
                }
                index++;
                //System.out.print("["+s+"] ");
            }
            if (pos != -1 && pos + 2 < str.length()) {
                if (!baseTypeSet.contains(strs[pos + 1])) {
                    continue;
                }
                System.out.print(strs[pos] + "," + strs[pos + 1] + "," + strs[pos + 2]);
                System.out.println();
                tmpList.add(strs[pos + 2].replace(";", ""));
            }
        }
        System.out.println(createInsert(tmpList));
        System.out.println(createFindBy(tmpList, "userName"));
    }

    /**
     * 创建批量插入
     *
     * @param list
     * @return
     */
    private static String createInsert(List<String> list) {
        String sql = "int batchInsert(List<?> list);\n" +
                "<insert id=\"batchInsert\" parameterType=\"java.util.List\">\n" +
                "        INSERT INFO attr_info ($1)\n" +
                "        VALUES\n" +
                "        <foreach collection=\"list\" item=\"item\" separator=\",\">\n" +
                "            ($2)\n" +
                "        </foreach>\n" +
                " </insert>";
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for (String s : list) {
            sb1.append(transToUnderLineStr(s) + ",");
            sb2.append("#{item." + s + "},");
        }
        sb1.deleteCharAt(sb1.length() - 1);
        sb2.deleteCharAt(sb2.length() - 1);
        sql = sql.replaceAll("\\$1", sb1.toString());
        sql = sql.replaceAll("\\$2", sb2.toString());
        return sql;
    }


    /**
     * 创建批量插入
     *
     * @param list
     * @return
     */
    private static String createFindBy(List<String> list, String name) {
        String tmpName = name.substring(0, 1).toUpperCase() + name.substring(1);
        String sql = "List<?> findBy" + tmpName + "(@Param(\"" + name + "\") String " + name + ");\n" +
                "<select id=\"findBy" + tmpName + "\" resultType=\"com.zsk.entity.AttrInfo\">\n" +
                "        SELECT \n$1 \n" +
                "        FROM attr_info \n" +
                "        WHERE " + transToUnderLineStr(name) + " = #{" + name + "}\n" +
                "</select>\n";
        StringBuilder sb1 = new StringBuilder();

        for (String s : list) {
            sb1.append("        " + transToUnderLineStr(s) + " as " + s + ",\n");
        }
        sb1.delete(sb1.length() - 2, sb1.length());
        sql = sql.replaceAll("\\$1", sb1.toString());
        return sql;
    }

    /**
     * 驼峰转下划线
     *
     * @param str
     * @return
     */
    public static String transToUnderLineStr(String str) {
        char[] cs = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : cs) {
            if (Character.isUpperCase(c)) {
                sb.append('_').append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 下划线转驼峰
     *
     * @param str
     * @return
     */
    public static String transToStr(String str) {
        char[] cs = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        int lastUnderLinePos = -2;
        int index = 0;
        for (char c : cs) {
            if (c == '_') {
                lastUnderLinePos = index;
            } else if (lastUnderLinePos + 1 == index) {
                sb.append(Character.toUpperCase(c));
            } else {
                sb.append(c);
            }
            index++;
        }
        return sb.toString();
    }
}
