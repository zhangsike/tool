package com.zsk.tool.io;

import java.io.Closeable;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * io 工具
 * 
 * @author keke
 * @date 2020/08/15
 */
public class IOUtil {

    private static final  Logger LOG = LoggerFactory.getLogger(IOUtil.class.getClass());

    public static void close(Closeable... ios) {
        for (Closeable closeable : ios) {
            try {
                closeable.close();
            } catch (IOException e) {
                LOG.error(e.toString());
            }
        }
    }
}
