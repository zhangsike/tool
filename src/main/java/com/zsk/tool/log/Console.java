package com.zsk.tool.log;

/**
 * 控制台输出日志
 * 
 * @author keke
 * @date 2020/08/15
 */
public class Console {
    public static void debug(Object o) {
        out(LogLevel.DEBUGGER, o);
    }

    public static void info(Object o) {
        out(LogLevel.INFO, o);
    }

    public static void warn(Object o) {
        out(LogLevel.WARN, o);
    }

    public static void error(Object o) {
        out(LogLevel.ERROR, o);
    }

    public static void out(LogLevel level, Object o) {
        if (level == LogLevel.DEBUGGER) {
            System.out.println(o);
        } else if (level == LogLevel.INFO) {
            System.out.println(o);
        } else if (level == LogLevel.WARN) {
            System.out.println(o);
        } else if (level == LogLevel.ERROR) {
            System.err.println(o);
        }
    }

}
