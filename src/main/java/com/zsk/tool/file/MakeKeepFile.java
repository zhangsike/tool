package com.zsk.tool.file;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 解决svn不能上传空文件加问题(在空目录下创建.keep文件)
 * 
 * @author keke
 * @date 2020/10/25
 */
public class MakeKeepFile {

    public static void main(String[] args) throws IOException {

        createKeepFileInEmptyDir("E:\\code\\springboot-guide\\springmvc-parent");

    }

    public static void createKeepFileInEmptyDir(String dirPath) throws IOException {

        File file = new File(dirPath);
        if (!file.isDirectory()) {
            return;
        }
        File[] fs = file.listFiles();
        Queue<File> queue = new LinkedList<>();
        for (File f : fs) {
            if (f.isDirectory()) {
                queue.offer(f);
                if (f.list().length == 0) {
                    File keepFile = new File(f.getAbsolutePath() + "/.keep");
                    keepFile.createNewFile();
                }
            }

        }
        while (!queue.isEmpty()) {
            File fileTemp = queue.poll();
            File[] fileListTemp = fileTemp.listFiles();
            for (File f : fileListTemp) {
                if (f.isDirectory()) {
                    queue.offer(f);
                    if (f.list().length == 0) {
                        File keepFile = new File(f.getAbsolutePath() + "/.keep");
                        keepFile.createNewFile();
                    }
                }
            }
        }
    }

}
