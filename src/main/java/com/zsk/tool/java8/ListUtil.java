package com.zsk.tool.java8;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: keke
 * @date: 2021/11/14 13:03
 */
public class ListUtil {

	public static void main(String[] args) {
		/*数组*************************************/
		int[] num = new int[]{1, 2, 3};
		// 1.数组转List
		List<Integer> list = Arrays.stream(num).boxed().collect(Collectors.toList());
		// 2.List转数组
		num = list.stream().mapToInt(Integer::intValue).toArray();
		// 3.初始化数组
		list = Stream.of(1, 2, 3).collect(Collectors.toList());
		list.add(4);
		System.out.println("3:" + list);

		/*String数组*************************************/

		String[] strs = new String[]{"c", "c++", "java"};
		// string数组转List
		List<String> stringList = Arrays.stream(strs).collect(Collectors.toList());
		// List转string数组
		String[] newStringArrays = stringList.toArray(new String[0]);
		System.out.println("4:" + Arrays.toString(newStringArrays));

		/*对象数组*************************************/
		List<User> userList = new ArrayList<>();
		userList.add(User.builder().name("zhangsan").age(10).build());
		userList.add(User.builder().name("lisi").age(20).build());
		// 4.提取对象数组一个属性转换成list
		List<String> nameList = userList.stream().map(User::getName).collect(Collectors.toList());
		System.out.println("5:" + nameList);
		// 5.提取对象数组提取两个属性作为K,V返回map
		Map<String, Integer> map = userList.stream().collect(Collectors.toMap(User::getName, user -> user.getAge()));
		System.out.println("6:" + map);
	}
}
