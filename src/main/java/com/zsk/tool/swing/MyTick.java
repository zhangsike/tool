package com.zsk.tool.swing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Random;

import javax.security.auth.x500.X500Principal;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyTick extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private volatile int v = 0;
    private Random random = new Random();

    public void init() {
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        addPanel();
        setVisible(true);
        validate();
        System.out.println(Math.asin(0.5));
    }

    private void addPanel() {
        JPanel panel = new JPanel() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                // TODO Auto-generated method stub
                super.paintComponent(g);
                int px = 300;
                int py = 300;
                int r = 100;
                Graphics2D g2 = (Graphics2D)g;
                g2.setColor(Color.RED);
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
               
                g2.drawArc(200, 200, 200, 200, 0, 180);

                double du = 180 / 100.0 * v;
                double hdu = Math.PI / 180.0 * du;

                if (du < 90) {
                    double y = Math.sin(hdu) * (r-10);
                    double x = Math.cos(hdu) * (r-10);
                    int xx = px - (int)x;
                    int yy = py - (int)y;
                    g2.drawLine(px, py, xx, yy);
                } else if (du == 90) {
                    g2.drawLine(px, py, px, py - r);
                } else {
                    hdu -= 90;
                    double y = Math.cos(hdu) * (r-10);
                    double x = Math.sin(hdu) * (r-10);
                    int xx = px + (int)x;
                    int yy = py - (int)y;
                    g2.drawLine(px, py, xx, yy);
                }

            }
        };
        add(panel);
        new Thread(() -> {
            while (true) {
                try {
                    v++;
                    if (v > 100) {
                        v = 0;
                    }
                    panel.repaint();
                    Thread.sleep(250);

                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void main(String[] args) {
        new MyTick().init();
    }

}
