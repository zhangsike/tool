package com.zsk.tool.ano;

import com.zsk.tool.http.HttpFactory;

import okhttp3.OkHttpClient;

/**
 * 
 * @author keke
 * @date 2020/11/14
 */
public class Http {
    public final static OkHttpClient HTTP = HttpFactory.createOkHttpClient(true);
}
