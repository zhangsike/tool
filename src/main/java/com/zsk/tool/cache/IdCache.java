package com.zsk.tool.cache;

import com.zsk.tool.ano.Http;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * https://blog.csdn.net/qq_27093465/article/details/121289837 定时任务
 *
 * @author: keke
 * @date: 2021/11/24 22:48
 */
public class IdCache {
    private static final Logger logger = LoggerFactory.getLogger(IdCache.class);
    private static final ConcurrentLinkedQueue<Long> queue = new ConcurrentLinkedQueue<>();
    private static final int total = 10;

    static {
        // 启动时缓存10个id
        CompletableFuture.runAsync(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    Request request = new Request.Builder().url("http://127.0.0.1:8080/test/getId").get().build();
                    String num = Http.HTTP.newCall(request).execute().body().string();
                    long id = Long.parseLong(num);
                    queue.offer(id);
                }
                logger.info("初始化id：" + queue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        // 3秒后 每隔1秒检查一次id个数，如果小于50%，则补充50%
        AtomicInteger index = new AtomicInteger(1);
        new Thread() {
            @Override
            public void run() {
                super.run();
                while (true) {
                    try {
                        logger.info("执行检查");
                        int half = total / 2;
                        if (queue.size() <= half) {
                            logger.info("id不足一半 {}", queue);
                            for (int i = 0; i < half; i++) {
                                cacheId();
                            }
                            logger.info("id不足一半 补充 {} 个,{}", half, queue);
                        }
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    public long getId() throws IOException {
        Long id = queue.poll();
        if (id != null) {
            logger.info("消费一个缓存id {}", id);
            return id;
        }
        id = getRemoteId();
        logger.info("消费一个远程id {}", id);
        return id;
    }

    private static long getRemoteId() throws IOException {
        try {
            Request request = new Request.Builder().url("http://127.0.0.1:8080/test/getId").get().build();
            String num = Http.HTTP.newCall(request).execute().body().string();
            long id = Long.parseLong(num);
            return id;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private static void cacheId() {
        try {
            long newId = getRemoteId();
            queue.offer(newId);
            //logger.info("补充一个新id到缓存：{},{}", newId, queue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        IdCache idCache = new IdCache();
        Thread.sleep(2000);
        new Thread() {
            @Override
            public void run() {
                super.run();
                int index = 100;
                while (true) {
                    if (index == 0) {
                        break;
                    }
                    try {
                        idCache.getId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    index--;
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
