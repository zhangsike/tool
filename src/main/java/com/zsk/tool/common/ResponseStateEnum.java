package com.zsk.tool.common;

public enum ResponseStateEnum {
    SUCCESS(200, "success"), FAIL(-1, "fail"),
    FAIL_NOT_FOUND_ERROR(404, "not found"),
    FAIL_SERVER_EXCEPTION_ERROR(500, "error occured in server,please contact the IT administrator"),
    FAIL_REQUEST_PARAM_ERROR(10000, "request param error"), 
    // add new error here
    
    FAIL_UNKNOWN_ERROR(19999, "unknown error,please contact the IT administrator");

    private int code;
    private String description;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private ResponseStateEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
