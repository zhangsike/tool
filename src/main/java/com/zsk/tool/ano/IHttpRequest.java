package com.zsk.tool.ano;

@RequestClient
public interface IHttpRequest {

    public final static String BASE = "http://www.baidu.com";

    @GetRequest(url = BASE + "?k=123")
    String getData();

    @GetRequest(url = "sd.com")
    String getData1();

    String getName();

}
