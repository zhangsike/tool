package com.zsk.tool.date;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * https://www.liaoxuefeng.com/wiki/1252599548343744
 *
 * @author: keke
 * @date: 2023/5/31
 */
public class DateTimeUtil {

	public final static DateTimeFormatter format_yyyy_MM_dd_hh_mm_ss = DateTimeFormatter.ofPattern("yyyy-MM-dd " + "hh"
			+ ":mm:ss");
	public final static DateTimeFormatter format_yyyy_MM_dd = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	/**
	 * @return 2023-05-31 10:44:01
	 */
	public static String yyyyMMddhhmmss() {
		return LocalDateTime.now().format(format_yyyy_MM_dd_hh_mm_ss);
	}

	/**
	 * @return 2023-05-31
	 */
	public static String yyyyMMdd() {
		return LocalDateTime.now().format(format_yyyy_MM_dd);
	}

	/**
	 * @param time
	 * @return 2023-05-31 10:44:01
	 */
	public static String yyyyMMddhhmmss(long time) {
		Instant instant = Instant.ofEpochMilli(time);
		ZoneId zone = ZoneId.systemDefault();
		return LocalDateTime.ofInstant(instant, zone).format(format_yyyy_MM_dd_hh_mm_ss);
	}

	/**
	 * @param time
	 * @return 2023-05-31
	 */
	public static String yyyyMMdd(long time) {
		Instant instant = Instant.ofEpochMilli(time);
		ZoneId zone = ZoneId.systemDefault();
		return LocalDateTime.ofInstant(instant, zone).format(format_yyyy_MM_dd);
	}

	/**
	 * unix标准时间转换
	 *
	 * @param time
	 * @return 2023-05-31
	 */
	public static String yyyyMMddUnix(long time) {
		Instant instant = Instant.ofEpochMilli(time);
		// ZoneId.systemDefault();
		System.out.println("ZoneId.systemDefault()="+ZoneId.systemDefault());
		ZoneId zone = ZoneId.of("Asia/Shanghai");
		zone = ZoneId.of("UTC+8");
		return LocalDateTime.ofInstant(instant, zone).format(format_yyyy_MM_dd_hh_mm_ss);
	}

}
