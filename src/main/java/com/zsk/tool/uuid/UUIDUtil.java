package com.zsk.tool.uuid;

import java.util.UUID;

import org.jetbrains.annotations.NotNull;

/**
 * @author keke
 * @date 2020/08/15
 */
public class UUIDUtil {

    /**
     * 获取一个不带横线的UUID
     *
     * @return
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 获取一个原始的UUID(带横线)
     *
     * @return
     */
    public static String uuidOrigin() {
        return UUID.randomUUID().toString();
    }
     
    public static void test(@NotNull String msg) {
        System.out.println(msg);
    }
    
    public static void main(String[] args) {
        test(null);
    }
}
