package com.zsk.test;

import org.junit.Test;

import com.zsk.tool.date.DateTimeUtil;
import com.zsk.tool.log.Console;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateTimeUtilTest {

    @Test
    public void test() {
        Console.info(DateTimeUtil.yyyyMMdd());
        Console.info(DateTimeUtil.yyyyMMddhhmmss());
        Console.info(DateTimeUtil.yyyyMMddhhmmss(System.currentTimeMillis()));
        Console.info(DateTimeUtil.yyyyMMdd(System.currentTimeMillis()));

        Date date = new Date();
        System.out.println(DateTimeUtil.yyyyMMddUnix(date.getTime()));
    }

}
