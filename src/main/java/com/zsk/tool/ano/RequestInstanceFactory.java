package com.zsk.tool.ano;

import java.lang.reflect.Proxy;

public class RequestInstanceFactory {

    /**
     * 创建RequetClient实例
     *
     * @param cls
     * @return
     */
    public <T> T getInstance(Class<T> cls) {
        RequestClient requestClient = cls.getAnnotation(RequestClient.class);
        if (!cls.isInterface()) {
            throw new RuntimeException(cls.getName() + " is not interface.");
        }
        if (requestClient == null) {
            throw new RuntimeException(cls.getName() + " must annotation as @RequestClient");
        }
        MethodProxy invocationHandler = new MethodProxy();
        Object object = Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, invocationHandler);
        return cls.cast(object);
    }

}
