package com.zsk.tool.file;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

public class ApacheCommonUtilExample {
    public static void main(String[] args) throws IOException {

        String path = "C:\\Users\\admin\\Desktop\\test1\\test.txt";
        String path1 = "C:\\Users\\admin\\Desktop\\test1.txt";
        File file = FileUtils.getFile(path);

        // 写文件-会自动创建文件夹
        FileUtils.write(file, "hello", StandardCharsets.UTF_8, true);

        // 复制文件(不是文件夹),新文件存在则创建,会抛异常
        FileUtils.copyFile(file, FileUtils.getFile(path1));

        // 复制整个文件夹 test1->test2，如果新文件夹不存在自动创建
        FileUtils.copyDirectory(FileUtils.getFile("C:\\Users\\admin\\Desktop\\test1"),
            FileUtils.getFile("C:\\Users\\admin\\Desktop\\test2"));

        // 复制整个文件夹到文件夹下 test1->test3，如果新文件夹不存在自动创建
        FileUtils.copyDirectoryToDirectory(FileUtils.getFile("C:\\Users\\admin\\Desktop\\test1"),
            FileUtils.getFile("C:\\Users\\admin\\Desktop\\test3"));

        // 会抛出异常，例如java.io.FileNotFoundException
        FileUtils.forceDelete(file);

        // 删除-不抛出异常
        FileUtils.deleteQuietly(file);

        // 删除文件夹
        FileUtils.deleteDirectory(file);
    }

}
