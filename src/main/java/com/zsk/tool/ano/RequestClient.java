 package com.zsk.tool.ano;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * @author keke
 * @date 2020/11/14
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface RequestClient {

}
