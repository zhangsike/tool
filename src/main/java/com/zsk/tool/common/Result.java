package com.zsk.tool.common;

/**
 * common response entity
 * 
 * @author keke
 * @date 2021/01/10
 */
public class Result<T> {

    private int code;
    private String msg;
    private T data;

    public Result() {
        super();
    }

    public Result(ResponseStateEnum state) {
        super();
        this.code = state.getCode();
        this.msg = state.getDescription();
    }

    public Result(int code, String msg, T data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Result<String> success() {
        return new Result<String>(ResponseStateEnum.SUCCESS.getCode(), ResponseStateEnum.SUCCESS.getDescription(), "");
    }

    public static <T> Result<T> success(T data) {
        return new Result<T>(ResponseStateEnum.SUCCESS.getCode(), ResponseStateEnum.SUCCESS.getDescription(), data);
    }

    public static <T> Result<T> success(String msg, T data) {
        return new Result<T>(ResponseStateEnum.SUCCESS.getCode(), msg, data);
    }

    public static Result<String> fail() {
        return new Result<String>(ResponseStateEnum.FAIL.getCode(), ResponseStateEnum.FAIL.getDescription(), "");
    }

    public static Result<String> fail(String msg) {
        return new Result<String>(ResponseStateEnum.FAIL.getCode(), msg, "");
    }

    public static Result<String> fail(ResponseStateEnum state) {
        return new Result<String>(state.getCode(), state.getDescription(), "");
    }

    public static <T> Result<T> fail(T data) {
        return new Result<T>(ResponseStateEnum.FAIL.getCode(), ResponseStateEnum.FAIL.getDescription(), data);
    }

    public static <T> Result<T> fail(int code, String msg, T data) {
        return new Result<T>(code, msg, data);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result [code=" + code + ", msg=" + msg + ", data=" + data + "]";
    }

    static class User {
        private String userName;
        private int age;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public User(String userName, int age) {
            super();
            this.userName = userName;
            this.age = age;
        }

        @Override
        public String toString() {
            return "User [userName=" + userName + ", age=" + age + "]";
        }

    }

    public static void main(String[] args) {
        Result<String> r1 = Result.success("111");
        Result<String> r2 = Result.success("hello", new String("23554"));
        Result<User> r3 = Result.success(new User("zhangsike", 30));
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);

        Result<String> r4 = Result.fail();
        Result<String> r5 = Result.fail("name is null");
        Result<String> r6 = Result.fail(ResponseStateEnum.FAIL_SERVER_EXCEPTION_ERROR);
        Result<User> r7 = Result.fail(800, "xxxx", new User("zhangsike", 30));
        System.out.println(r4);
        System.out.println(r5);
        System.out.println(r6);
        System.out.println(r7);
    }

}
