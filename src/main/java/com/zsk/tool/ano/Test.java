package com.zsk.tool.ano;

public class Test {
    public static void main(String[] args) {
        RequestInstanceFactory factory = new RequestInstanceFactory();
        // 生成接口实例
        IHttpRequest httpRequest = factory.getInstance(IHttpRequest.class);
        System.out.println(httpRequest.getData());
        System.out.println(httpRequest.getName());
    }

}
